# 使用简单的左右滑动并且可以撤回的仿探探界面  
  
#### 介绍
使用简单的左右滑动并且可以撤回的仿探探界面，此为狗尾续貂之作，先列出大佬原作连接：https://github.com/xmuSistone/CardSlidePanel  
-----------------------------------------------------------------------------------------------------------------------
原作只有手势左右滑动，但需求还要按键删除和撤回，故在原作上加以补充，添加了几个功能:  
----------------------------------------------------------------------------

1.手势左右滑动删除（原作已有)  
  
2.每张卡片上新增对应功能按钮（可按需配置，左删，右删，撤回）  
  
3.可以在控件外任意地方调用方法进行操作（左删，右删，撤回）  
  
4.丰富的回调，可动态控制功能按钮(可动态开关指定功能，自由组合)  

5.新增了卡片拖拽距离边上百分比的回调,具体回调见CardSlidePanel.CardSwitchListener.onCardMove()  
  
效果图,(看图应该很容易看出卡片上按钮与卡片外按钮的区别，如果此处图片不能正常显示，可点击image文件夹里的效果图查看)  
---------------------------------------------------------------------------------------------------------
![效果图](https://gitee.com/belong_z/CardSlip/raw/master/image/%E6%95%88%E6%9E%9C%E5%9B%BE.gif)

#### 使用方法  

1. File-->New--->Import Module 选择此项目中的 “library” 以Model的方式导入到你的项目  
记得在 app build.gradle--->dependencies 中添加implementation project(':library')  

2. 在Xml布局中使用CardSlidePanel控件  

3. 设计卡片布局 新建一个布局文件 注意你需要的功能 (基本功能是手势左右滑动进行删除) 可添加额外功能按钮  
如：在每张卡片上需要一个向右滑动删除的功能按钮，即可在布局中设置个Button  
    然后给对应id “ android:id="@id/touch_right" ”（注意，没有+id）  
亦可在每张卡片之外的其他按钮调用对应方法  
功能---->每张卡片功能控件id----->单独每张卡片外其他地方调用方法  
向左删除---> touch_left ----->cardSlidePanel.delItem(CardSlidePanel.VANISH_TYPE_LEFT);  
向右删除---> touch_right----->cardSlidePanel.delItem(CardSlidePanel.VANISH_TYPE_RIGHT);  
撤回上一个----> touch_retract----->cardSlidePanel.retractItem();  

例子：需要一个卡片上有左删除，右删除，撤回三个功能按钮  
   卡片布局大概应该这样(省略了一些代码，注意id与功能)   

```xml
<RelativeLayout 
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <ImageView/>
    <Button
        android:id="@id/touch_left"
        android:text="左边"
        />
    <Button
        android:id="@id/touch_retract"
        android:text="撤回"
        />
    <Button
        android:id="@id/touch_right"
        android:text="右边"
        />
</RelativeLayout>
```
  
4. 设置Adapter 使用你的卡片布局设置CardAdapter适配器  
```java
 CardSlidePanel mCardSlidePanel.setAdapter(new CardAdapter() {
            @Override
            public int getLayoutId() {
                return R.layout.card_item;
            }

            @Override
            public int getCount() {
                return mCardDataItems.size();
            }

            @Override
            public void bindView(View view, int index) {
                Object tag = view.getTag();
                ViewHolder viewHolder;
                if (null != tag) {
                    viewHolder = (ViewHolder) tag;
                } else {
                    viewHolder = new ViewHolder(view);
                    view.setTag(viewHolder);
                }
                viewHolder.userNameTv(mCardDataItems.get(index).getUserName());
            }

            @Override
            public Object getItem(int index) {
                return mCardDataItems.get(index);
            }
        });
```

4. 设置监听  
```java
 CardSlidePanel mCardSlidePanel.setCardSwitchListener(new CardSlidePanel.CardSwitchListener() {
             /**
             * 新卡片显示回调
             * @param index 最顶层显示的卡片的index
             */
            @Override
            public void onShow(int index) {
                Log.e("Card", "正在显示- "+index +" ----"+ mCardDataItems.get(index).getUserName());
            }

            /**
             * 卡片飞向两侧回调
             * @param index 飞向两侧的卡片数据index
             * @param type  飞向哪一侧{@link #VANISH_TYPE_LEFT}或{@link #VANISH_TYPE_RIGHT}
             */
            @Override
            public void onCardVanish(int index, int type) {
                Log.e("Card", "正在消失-" + mCardDataItems.get(index).getUserName() + " 消失type=" + type);
            }
            /**
             * 卡片撤回的回调
             * @param status :1：撤回成功 2：已经没有可以撤回的数据
             * @param type :之前飞向哪一侧{@link #VANISH_TYPE_LEFT}或{@link #VANISH_TYPE_RIGHT}
             */
            @Override
            public void onCardRetract(int status, int type) {
                Log.e("Card", "正在撤回-"+(status==1?"撤回成功 ":"已经没有可以撤回的数据") + " 撤回type=" + type);
            }
            /**
             * 卡片功能按钮的监听
             * @return :
             *      0: off  1: on
             *          撤 左 右
             *      0b  0  0  0
             *
             *      列：只能撤回--->0b100
             */
            @Override
            public int onFunctionEnabled() {
                return 0b111;
            }
            /**
             * 卡片移动距离的回调
             *
             * @param percentage:移动距离的百分比：中心--->删除  0---->1  -1:为松开手指 回到中心
             * @param moveCard：移动的卡片
             */
            @Override
            public void onCardMove(float percentage, View moveCard) {
                Log.e("Card", "移动距离百分比---="+percentage);
            }
        });
```
5. 大功告成，愉快使用  
-------------------
  
 #### 参与贡献
1. 感谢原作者对基础功能的搭建，再次放上原作连接：https://github.com/xmuSistone/CardSlidePanel
  