package com.example.cardslip;

/**
 * 卡片数据装载对象
 *
 * @author xmuSistone
 */
public class CardDataItem {
    private String imagePath;
    private String userName;
    private int likeNum;
    private int imageNum;

    public CardDataItem(String imagePath, String userName) {
        this.imagePath = imagePath;
        this.userName = userName;
        this.likeNum = 1;
        this.imageNum = 1;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }

    public int getImageNum() {
        return imageNum;
    }

    public void setImageNum(int imageNum) {
        this.imageNum = imageNum;
    }
}
