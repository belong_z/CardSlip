package com.example.cardslip;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stone.card.library.CardAdapter;
import com.stone.card.library.CardSlidePanel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private CardSlidePanel mCardSlidePanel;
    private Random random = new Random();
    private String imagePaths[] = {"wall01.jpg", "wall02.jpg", "wall03.jpg",
            "wall04.jpg", "wall05.jpg", "wall06.jpg", "wall07.jpg",
            "wall08.jpg", "wall09.jpg", "wall10.jpg", "wall11.jpg", "wall12.jpg"}; // 12个图片资源
    private ArrayList<CardDataItem> mCardDataItems;
    private CardAdapter mAdapter;
    private int dataIndex=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        initData();
        mCardSlidePanel = findViewById(R.id.csp_);
        mAdapter = new CardAdapter() {
            @Override
            public int getLayoutId() {
                return R.layout.card_item;
            }

            @Override
            public int getCount() {
                return mCardDataItems.size();
            }

            @Override
            public void bindView(View view, int index) {
                Object tag = view.getTag();
                ViewHolder viewHolder;
                if (null != tag) {
                    viewHolder = (ViewHolder) tag;
                } else {
                    viewHolder = new ViewHolder(view);
                    view.setTag(viewHolder);
                }
                viewHolder.bindData(mCardDataItems.get(index));
            }

            @Override
            public Object getItem(int index) {
                return mCardDataItems.get(index);
            }
        };
        mCardSlidePanel.setAdapter(mAdapter);

        mCardSlidePanel.setCardSwitchListener(new CardSlidePanel.CardSwitchListener() {
            /**
             * 新卡片显示回调
             * @param index 最顶层显示的卡片的index
             */
            @Override
            public void onShow(int index) {
                Log.e("Card", "正在显示- "+index +" ----"+ mCardDataItems.get(index).getUserName());
                if (index==mCardDataItems.size()-2){
                    addData();
                }
            }

            /**
             * 卡片飞向两侧回调
             * @param index 飞向两侧的卡片数据index
             * @param type  飞向哪一侧{@link #VANISH_TYPE_LEFT}或{@link #VANISH_TYPE_RIGHT}
             */
            @Override
            public void onCardVanish(int index, int type) {
                Log.e("Card", "正在消失-" + mCardDataItems.get(index).getUserName() + " 消失type=" + type);
            }
            /**
             * 卡片撤回的回调
             * @param status :1：撤回成功 2：已经没有可以撤回的数据
             * @param type :之前飞向哪一侧{@link #VANISH_TYPE_LEFT}或{@link #VANISH_TYPE_RIGHT}
             */
            @Override
            public void onCardRetract(int status, int type) {
                Log.e("Card", "正在撤回-"+(status==1?"撤回成功 ":"已经没有可以撤回的数据") + " 撤回type=" + type);
            }
            /**
             * 卡片功能按钮的监听
             * @return :
             *      0: off  1: on
             *          撤 左 右
             *      0b  0  0  0
             *
             *      列：只能撤回--->0b100
             */
            @Override
            public int onFunctionEnabled() {
                return 0b111;
            }
            /**
             * 卡片移动距离的回调
             *
             * @param percentage:移动距离的百分比：
             *                  -1:为松开手指 回到中心
             *                  中心--->删除  0---->1
             *                  +-表示方向
             *                  +：右移   -：左移
             * @param oldCard：移动的卡片下面一层的CardView
             * @param moveCard：移动的卡片
             */
            @Override
            public void onCardMove(float percentage, View oldCard,  View moveCard) {
                Log.e("Card", "移动距离百分比---="+percentage);
                if (oldCard!=null){
                    oldCard.findViewById(R.id.hide_view).setAlpha(0);
                }
                View hide_view = moveCard.findViewById(R.id.hide_view);
                hide_view.setAlpha(percentage==-1?0:Math.abs(percentage));
            }

        });
    }

    private void initData() {
        mCardDataItems = new ArrayList<>();
        addData();
    }

    private void addData() {
        for (int i = 0; i < 6; i++) {
            mCardDataItems.add(new CardDataItem(imagePaths[random.nextInt(12)],("这个还行"+dataIndex)));
            dataIndex++;
        }
        if (mAdapter!=null){
            mAdapter.notifyDataSetChanged();
        }
    }

    public void clickFun(View view) {
        switch (view.getId()) {
            case R.id.btn_1://左删除
                mCardSlidePanel.delItem(CardSlidePanel.VANISH_TYPE_LEFT);
                break;
            case R.id.btn_2://撤回
                mCardSlidePanel.retractItem();
                break;
            case R.id.btn_3://右删除
                mCardSlidePanel.delItem(CardSlidePanel.VANISH_TYPE_RIGHT);
                break;
            case R.id.btn_4://加载更多数据
                addData();
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    class ViewHolder {
        View hide_view;
        ImageView imageView;
        View maskView;
        TextView userNameTv;
        TextView imageNumTv;
        TextView likeNumTv;

        public ViewHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.card_image_view);
            maskView = view.findViewById(R.id.maskView);
            userNameTv = (TextView) view.findViewById(R.id.card_user_name);
            imageNumTv = (TextView) view.findViewById(R.id.card_pic_num);
            likeNumTv = (TextView) view.findViewById(R.id.card_like);
            hide_view =  view.findViewById(R.id.hide_view);
        }

        public void bindData(CardDataItem itemData) {
            try {
                InputStream inputStream = getAssets().open(itemData.getImagePath());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            userNameTv.setText(itemData.getUserName());
            imageNumTv.setText(itemData.getImageNum()+"");
            likeNumTv.setText(itemData.getLikeNum()+"");
            hide_view.setAlpha(0);
        }
    }

}
